<?php 
$this->load->view('include/header');
?>
<!-- Content -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-sitemap"></em>
			</a></li>
			<li class="active">Angsuran / Edit</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Petugas</h2>
			<?=$this->session->flashdata('notif')?>
		</div>
	</div><!--/.row-->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Edit Data Petugas
					<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
					<?php foreach($tb_pinjaman_kategori as $data){ ?>   
						<form role="form" method="post" action="<?php echo base_url().'Kategori/Edit_kategori' ?>">
							<div class="col-md-6">
								<div class="form-group">
									<label for="nama">Nama</label>
									<input type="text" class="form-control" id="nama" name="nama" value="<?php echo $data->nama_pinjaman_kategori; ?>" autocomplete="off">
									<input type="hidden" class="form-control" id="id_petugas" name="id_pinjaman_kategori" value="<?php echo $data->id_pinjaman_kategori; ?>" placeholder="Masukkan Nama" autocomplete="off">
									<strong><?php echo form_error('nama'); ?></strong>
								</div>	
							
							
								<button type="submit" name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Simpan
								</button>
								<a href="<?php echo base_url(); ?>Kategori/Batal/" class="btn btn-warning" data-confirm2="Apakah anda yakin akan membatalkan operasi ini ?"><span class="glyphicon glyphicon-remove"></span> Batal</a>
							</div>
							</div>
						</form>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>


	<?php 
	$this->load->view('include/footer');
	?> 						