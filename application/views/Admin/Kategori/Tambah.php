<?php 
$this->load->view('include/header');
?>
<!-- Content -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-sitemap"></em>
			</a></li>
			<li class="active">Kategori / Tambah</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Kategori</h2>
			<?=$this->session->flashdata('notif')?>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
		<div class="panel-heading">
			Tambah Data Kategori
			<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span>
		</div>
		<div class="panel-body">
			<div class="col-md-6">
				<form role="form" method="post" action="<?php echo base_url().'Kategori/Tambah_kategori' ?>">
					<div class="form-group">
						<label for="nama">Kategori Pinjaman</label>
						<input type="text" class="form-control" id="nama" name="nama" value="<?= set_value('nama'); ?>" placeholder="Masukkan Kategori" autocomplete="off">
						<strong><?php echo form_error('nama'); ?></strong>
					</div>								
					<button type="submit" name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Simpan
					</button>
					<a href="<?php echo base_url(); ?>Anggota/Batal/" class="btn btn-warning" data-confirm2="Apakah anda yakin akan membatalkan operasi ini ?"><span class="glyphicon glyphicon-remove"></span> Batal</a>
				</div>
			</form>
		</div>
	</div>
</div>


<?php 
$this->load->view('include/footer');
?> 						