<?php 
$this->load->view('include/header');
?>
<!-- Content -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-sitemap"></em>
			</a></li>
			<li class="active">Petugas / Edit</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Petugas</h2>
			<?=$this->session->flashdata('notif')?>
		</div>
	</div><!--/.row-->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Edit Data Petugas
					<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
					<?php foreach($tb_petugas as $data){ ?>   
						<form role="form" method="post" action="<?php echo base_url().'Petugas/Edit_petugas' ?>">
							<div class="col-md-6">
								<div class="form-group">
									<label for="nama">Nama</label>
									<input type="text" class="form-control" id="nama" name="nama" value="<?php echo $data->nama_petugas; ?>" placeholder="Masukkan Nama" autocomplete="off">
									<input type="hidden" class="form-control" id="id_petugas" name="id_petugas" value="<?php echo $data->id_petugas; ?>" placeholder="Masukkan Nama" autocomplete="off">
									<strong><?php echo form_error('nama'); ?></strong>
								</div>	
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="no_telp">No.Telp</label>
									<input type="int" class="form-control" id="no_telp" name="no_telp" value="<?php echo $data->no_telp_petugas; ?>" autocomplete="off">
									<strong><?php echo form_error('no_telp'); ?></strong>
								</div>	
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="tempat">Tempat Lahir</label>
									<input type="text" class="form-control" id="tempat" name="tempat" value="<?php echo $data->tempat_lahir_petugas; ?>" placeholder="Masukkan Nama" autocomplete="off">
									<strong><?php echo form_error('tempat'); ?></strong>
								</div>	
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="tanggal">Tanggal Lahir</label>
									<input type="date" class="form-control" id="tanggal" name="tanggal" value="<?php echo $data->tanggal_lahir_petugas; ?>" placeholder="Masukkan Nama" autocomplete="off">
									<strong><?php echo form_error('tanggal'); ?></strong>
									<span class="label label-success">Hari /Bulan /Tahun</span>
								</div>	
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="alamat">Alamat</label>
									<textarea class="form-control" id="alamat" name="alamat" value="<?php echo $data->alamat_petugas; ?>" autocomplete="off"><?php echo $data->alamat_petugas; ?></textarea>
									<strong><?php echo form_error('alamat'); ?></strong>
								</div>	
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="keterangan">Keterangan</label>
									<textarea class="form-control" id="keterangan" name="keterangan" value="<?php echo $data->keterangan_petugas; ?>" placeholder="Masukkan Nama" autocomplete="off"><?php echo $data->keterangan_petugas; ?></textarea>
									<strong><?php echo form_error('keterangan'); ?></strong>
								</div>	
								<button type="submit" name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Simpan
								</button>
								<a href="<?php echo base_url(); ?>Anggota/Batal/" class="btn btn-warning" data-confirm2="Apakah anda yakin akan membatalkan operasi ini ?"><span class="glyphicon glyphicon-remove"></span> Batal</a>
							</div>
						</form>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>


	<?php 
	$this->load->view('include/footer');
	?> 						