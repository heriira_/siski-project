<?php 
$this->load->view('include/header');
 ?>

<!-- Content -->
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-sitemap"></em>
				</a></li>
				<li class="active">Petugas</li>
				<li class="active">Lihat</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">Petugas</h2>
			</div>
		</div><!--/.row-->
		
		
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
											<?php foreach($tb_petugas as $data){ ?> 
						<?php echo "Data lengkap Anggota dengan nama " . "<strong><i>" . $data->nama_petugas . "</i></strong>"; ?>
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
						<table class="table">
						    <tr><td>Id</td><td><?php echo $data->id_petugas; ?></td></tr>
						    <tr><td>Nama Anggota</td><td><?php echo $data->nama_petugas; ?></td></tr>
						    <tr><td>Tempat Lahir</td><td><?php echo $data->tempat_lahir_petugas; ?></td></tr>
						    <tr><td>Taggal Lahir</td><td><?php echo $data->tanggal_lahir_petugas; ?></td></tr>
						    <tr><td>No.Telp</td><td><?php echo $data->no_telp_petugas; ?></td></tr>
						    <tr><td>Alamat</td><td><?php echo $data->alamat_petugas; ?></td></tr>
						    <tr><td>Keterangan</td><td><?php echo $data->keterangan_petugas; ?></td></tr>
						    <tr><td></td><td><a href="<?php echo site_url('Petugas') ?>" class="btn btn-default"><em class="fa fa-arrow-circle-left"></em> Kembali</a></td></tr>
						</table>
					</div>
					<?php }?>
				</div>
			</div>
		</div><!--/.row-->
		<!-- End Content -->

<?php 
$this->load->view('include/footer');
 ?>