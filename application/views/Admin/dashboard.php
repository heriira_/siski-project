<?php
$this->load->view('include/header');
?>			
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-home"></em>
			</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Dashboard</h1>
		</div>
	</div><!--/.row-->

	<div class="panel panel-container">
		<div class="row">
			<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
				<div class="panel panel-teal panel-widget border-right">
					<div class="row no-padding"><em class="fa fa-xl fa-shopping-cart color-blue"></em>
						<div class="large"><?php echo $this->M_anggota->get_data('tb_angsuran')->num_rows(); ?></div>
						<div class="text-muted">Angsuran</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
				<div class="panel panel-blue panel-widget border-right">
					<div class="row no-padding"><em class="fa fa-xl fa-comments color-orange"></em>
						<div class="large"><?php echo $this->M_anggota->get_data('tb_pinjaman_kategori')->num_rows(); ?></div>
						<div class="text-muted">Kategori</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
				<div class="panel panel-orange panel-widget border-right">
					<div class="row no-padding"><em class="fa fa-xl fa-users color-teal"></em>
						<div class="large"><?php echo $this->M_anggota->get_data('tb_anggota')->num_rows(); ?></div>
						<div class="text-muted">Jumlah Anggota</div>
					</div>
				</div>
			</div>
			<div class="col-xs-6 col-md-3 col-lg-3 no-padding">
				<div class="panel panel-red panel-widget ">
					<div class="row no-padding"><em class="fa fa-xl fa-user color-red"></em>
						<div class="large"><?php echo $this->M_anggota->get_data('tb_petugas')->num_rows(); ?></div>
						<div class="text-muted">Petugas</div>
					</div>
				</div>
			</div>
		</div><!--/.row-->
	</div>
<?php
$this->load->view('include/footer');
?>	