<?php 
$this->load->view('include/header');
?>
<!-- Content -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-sitemap"></em>
			</a></li>
			<li class="active">Anggota / Tambah</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Anggota</h2>
			<?=$this->session->flashdata('notif')?>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
		<div class="panel-heading">
			Tambah Data Anggota
			<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span>
		</div>
		<div class="panel-body">
			<div class="col-md-6">
				<form role="form" method="post" action="<?php echo base_url().'Anggota/Tambah_anggota' ?>">
					<div class="form-group">
						<label for="nama">Nama</label>
						<input type="text" class="form-control" id="nama" name="nama" value="<?= set_value('nama'); ?>" placeholder="Masukkan Nama" autocomplete="off">
						<strong><?php echo form_error('nama'); ?></strong>
					</div>								
					<div class="form-group">
						<label for="tempat">Tempat Lahir</label>
						<input type="text" class="form-control" id="tempat" name="tempat" value="<?= set_value('tempat'); ?>" placeholder="Tempat Lahir" autocomplete="off">
						<strong><?php echo form_error('tempat'); ?></strong>
					</div>
					<div class="form-group">
						<label for="tanggal">Tanggal Lahir</label>
						<input type="date" class="form-control" id="tanggal" name="tanggal" value="<?= set_value('tanggal'); ?>" placeholder="Tanggal Lahir" autocomplete="off">
						<strong><?php echo form_error('tanggal'); ?></strong>
						<span class="label label-success">Bulan /Hari /Tahun</span>
					</div>	

					<div class="form-group">
						<label>Jenis Kelamin</label>
						<select class="form-control" id="jk" name="jk" value="<?= set_value('jk'); ?>" autocomplete="off">
							<option value="">-Jenis Kelamin-</option>
							<option value="Pria">Pria</option>
							<option value="Wanita">Wanita</option>
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="no_telp">No.Telp</label>
						<input type="int" class="form-control" id="no_telp" name="no_telp" value="<?= set_value('no_telp'); ?>" placeholder="Nomor Telephone" autocomplete="off">
						<strong><?php echo form_error('no_telp'); ?></strong>
					</div>	
					<div class="form-group">
						<label for="status">Status</label>
						<input type="text" class="form-control" id="status" name="status" value="<?= set_value('status'); ?>" placeholder="Status Anggota" autocomplete="off">
						<strong><?php echo form_error('status'); ?></strong>
					</div>	
					<div class="form-group">
						<label for="alamat">Alamat</label>
						<textarea class="form-control" id="alamat" name="alamat" value="<?= set_value('alamat'); ?>" placeholder="Alamat Anggota" autocomplete="off"></textarea>
						<strong><?php echo form_error('alamat'); ?></strong>
					</div>	
					<div class="form-group">
						<label for="keterangan">Keterangan</label>
						<textarea class="form-control" id="keterangan" name="keterangan" value="<?= set_value('keterangan'); ?>" placeholder="Keterangan Anggota" autocomplete="off"></textarea>
						<strong><?php echo form_error('keterangan'); ?></strong>
					</div>	
					<button type="submit" name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Simpan
					</button>
					<a href="<?php echo base_url(); ?>Anggota/Batal/" class="btn btn-warning" data-confirm2="Apakah anda yakin akan membatalkan operasi ini ?"><span class="glyphicon glyphicon-remove"></span> Batal</a>
				</div>
			</form>
		</div>
	</div>
</div>


<?php 
$this->load->view('include/footer');
?> 						