<?php 
$this->load->view('include/header');
 ?>

<!-- Content -->
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="#">
					<em class="fa fa-sitemap"></em>
				</a></li>
				<li class="active">Anggota</li>
				<li class="active">Lihat</li>
			</ol>
		</div><!--/.row-->
		
		<div class="row">
			<div class="col-lg-12">
				<h2 class="page-header">Anggota</h2>
			</div>
		</div><!--/.row-->
		
		
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading">
											<?php foreach($tb_anggota as $data){ ?> 
						<?php echo "Data lengkap Anggota dengan nama " . "<strong><i>" . $data->nama_anggota . "</i></strong>"; ?>
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
						<table class="table">
						    <tr><td>Id</td><td><?php echo $data->id_anggota; ?></td></tr>
						    <tr><td>Nama Anggota</td><td><?php echo $data->nama_anggota; ?></td></tr>
						    <tr><td>Tempat Lahir</td><td><?php echo $data->tempat_lahir_anggota; ?></td></tr>
						    <tr><td>Taggal Lahir</td><td><?php echo $data->tanggal_lahir_anggota; ?></td></tr>
						    <tr><td>Jenis kelamin</td><td><?php echo $data->jenis_kelamin_anggota; ?></td></tr>
						    <tr><td>Status</td><td><?php echo $data->status_anggota; ?></td></tr>
						    <tr><td>No.Telp</td><td><?php echo $data->no_telp_anggota; ?></td></tr>
						    <tr><td>Alamat</td><td><?php echo $data->alamat_anggota; ?></td></tr>
						    <tr><td>Keterangan</td><td><?php echo $data->keterangan_anggota; ?></td></tr>
						    <tr><td></td><td><a href="<?php echo site_url('Anggota') ?>" class="btn btn-default"><em class="fa fa-arrow-circle-left"></em> Kembali</a></td></tr>
						</table>
					</div>
					<?php }?>
				</div>
			</div>
		</div><!--/.row-->
		<!-- End Content -->

<?php 
$this->load->view('include/footer');
 ?>