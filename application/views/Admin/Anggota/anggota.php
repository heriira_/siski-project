<?php 
$this->load->view('include/header');
 ?>
 <!-- Content -->
 <title>Sistem Koperasi - Angggota</title>
 <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
 	<div class="row">
 		<ol class="breadcrumb">
 			<li><a href="#">
 				<em class="fa fa-sitemap"></em>
 			</a></li>
 			<li class="active">Anggota</li>
 		</ol>
 	</div><!--/.row-->

 	<div class="row">
 		<div class="col-lg-12">
 			<h2 class="page-header">Anggota</h2>
 			<div class="form-group text-right">
 				<a href="<?php echo base_url(); ?>Anggota/Tambah" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Anggota</a>
 			</div>
 			<?=$this->session->flashdata('notif')?>
 		</div>
 	</div><!--/.row-->
 	<div class="row">
 		<div class="col-md-12">
 			<div class="panel panel-default">
 				<div class="panel-heading">
 					Data Anggota
 					<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
 					<div class="panel-body">
 						<table id="customers2">
							<thead>
								<tr>
									<th><center>Id</th>
									<th><center>Nama</th>
									<th><center>Tempat</th>
									<th><center>TGL Lahir</th>
									<th><center>JK</th>
									<th><center>Status</th>
									<th><center>No.Telp</th>
									<th><center>Alamat</th>
									<th><center>Keterangan Anggota</th>
									<th><center>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$id = $this->uri->segment('3') + 1;
								if( ! empty($tb_anggota)){
								  foreach($tb_anggota as $data){ 
								    echo "<tr>";
								    echo "<td><center>".$id++." </td>";
								    echo "<td><center>".$data['nama_anggota']."</td>";
								    echo "<td><center>".$data['tempat_lahir_anggota']."</td>";
								    echo "<td><center>".$data['tanggal_lahir_anggota']."</td>";
								    echo "<td><center>".$data['jenis_kelamin_anggota']."</td>";
								    echo "<td><center>".$data['status_anggota']."</td>";
								    echo "<td><center>".$data['no_telp_anggota']."</td>";
								    echo "<td><center>".$data['alamat_anggota']."</td>";
								    echo "<td><center>".$data['keterangan_anggota']."</td>";
								    ?>
								    <td>
								    	<center>
								    	<ul class="pull panel-settings panel-button-tab">
											<li class="dropdown"><a class="pull dropdown-toggle" data-toggle="dropdown" href="#">
												<em class="fa fa-cogs" style="margin-left: 12px;"></em>
													</a>
														<ul class="dropdown-menu dropdown-menu-right">
															<li>
																<ul class="dropdown-settings">
																	<li><a href="<?php echo base_url(); ?>Anggota/Lihat/<?php echo $data['id_anggota']?>">
																		<em class="fa fa-eye"></em> Lihat
																	</a></li>
																	<li class="divider"></li>
																	<li><a href="<?php echo base_url(); ?>Anggota/Edit/<?php echo $data['id_anggota']?>">
																		<em class="fa fa-pencil"></em> Edit
																	</a></li>
																	<li class="divider"></li>
																	<li><a href="<?php echo base_url(); ?>Anggota/Delete/<?php echo $data['id_anggota']?>" data-confirm="Apakah anda benar-benar yakin akan menghapus data Anggota dengan nama <?php echo $data['nama_anggota'] ?> ?"><em class="fa fa-trash"></em> Hapus</a></li>
																</ul>
															</li>
														</ul>
													</li>
												</ul>
											</td>
											<?php
											echo "</tr>";
										}
									}else{ 
										echo "<tr><td colspan='9'><center>Data kosong.</center></td></tr>";
									}
									?>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div><!--/.row-->
			<!-- End Content -->
<?php 
$this->load->view('include/footer');
 ?>
 <script type="text/javascript">
    $(document).ready(function(){
        $('#customers2').DataTable({
        	"ordering": false,
        	"language":{
        		"url":"indonesia.json",
        		"sEmptyTable":"Tidads"
        	}
        });
    });
 </script>