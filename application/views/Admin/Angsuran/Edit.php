<?php 
$this->load->view('include/header');
?>
<!-- Content -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-sitemap"></em>
			</a></li>
			<li class="active">Angsuran / Edit</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Angsuran</h2>
			<?=$this->session->flashdata('notif')?>
		</div>
	</div><!--/.row-->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Edit Data Angsuran
					<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
					<?php foreach($tb_angsuran as $data){ ?>   
						<form role="form" method="post" action="<?php echo base_url().'Angsuran/Edit_angsuran' ?>">
							<div class="col-md-6">
								<input type="hidden" class="form-control" id="id_angsuran" name="id_angsuran" value="<?php echo $data->id_angsuran; ?>" placeholder="Masukkan Nama" autocomplete="off">
								<div class="form-group"><label>Id Pinjaman</label>   
									<select name="pinjaman" class="form-control" disabled>    
										<option value="">-Pilih ID-</option>    
										<?php foreach($tb_pinjaman as $k){ ?>
											<option <?php if($data->id_pinjaman == $k->id_pinjaman){echo "selected='selected'";} ?> value="<?php echo $k->id_pinjaman; ?>">
												<?php echo $k->id_pinjaman; ?></option>
											<?php } ?>    
										</select>    
										<?php echo form_error('pinjaman'); ?> 
									</div>
									<div class="form-group"><label>Nama Peminjam</label>   
									<select name="anggota" class="form-control" disabled>    
										<option value="">-Pilih Anggota-</option>    
										<?php foreach($tb_angsuran as $k){ ?>
											<option <?php if($data->nama_peminjam == $k->nama_peminjam){echo "selected='selected'";} ?> value="<?php echo $k->nama_peminjam; ?>">
												<?php echo $k->nama_peminjam; ?></option>
											<?php } ?>    
										</select>    
										<?php echo form_error('anggota'); ?> 
									</div>

								<div class="form-group">
									<label for="tanggal">Tanggal Pembayaran</label>
									<input type="date" class="form-control" id="tanggal" name="tanggal" value="<?php echo $data->tanggal_pembayaran; ?>" autocomplete="off">
									<strong><?php echo form_error('tanggal'); ?></strong>
								<span class="label label-success">Bulan /Hari /Tahun</span>									
								</div>
								<div class="form-group">
									<label for="tanggal">Tanggal Jatuh Tempo</label>
									<input type="date" class="form-control" id="tempo" name="tempo" value="<?php echo $data->tanggal_jatuh_tempo; ?>" autocomplete="off">
									<strong><?php echo form_error('tempo'); ?></strong>
								<span class="label label-success">Bulan /Hari /Tahun</span>									
								</div>

								</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Angsuran Ke</label>   
									<select name="angsuran" class="form-control">    
										<option value="">-Pilih Kategori-</option>    
										<?php foreach($tb_pinjaman_kategori as $k){ ?>
											<option <?php if($data->angsuran_ke == $k->nama_pinjaman_kategori){echo "selected='selected'";} ?> value="<?php echo $k->nama_pinjaman_kategori; ?>">
												<?php echo $k->nama_pinjaman_kategori; ?></option>
											<?php } ?>    
										</select>    
										<?php echo form_error('angsuran'); ?> 
									</div>
					<div class="form-group">
						<label for="besar">Besar Angsuran</label>
						<input type="text" class="form-control" id="besar" name="besar" value="<?php echo $data->besar_angsuran; ?>" placeholder="Nominal Angsuran" autocomplete="off">
						<strong><?php echo form_error('besar'); ?></strong>
					</div>
				<div class="form-group">
						<label for="keterangan">Keterangan Angsuran</label>
						<textarea class="form-control" id="keterangan" name="keterangan" value="<?php echo $data->keterangan_angsuran; ?>" placeholder="Keterangan Anggota" autocomplete="off"><?php echo $data->keterangan_angsuran; ?></textarea>
						<strong><?php echo form_error('keterangan'); ?></strong>
					</div>
				</div>
								<button type="submit" name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Simpan
								</button>
								<a href="<?php echo base_url(); ?>Angsuran/Batal/" class="btn btn-warning" data-confirm2="Apakah anda yakin akan membatalkan operasi ini ?"><span class="glyphicon glyphicon-remove"></span> Batal</a>
													</form>
					<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</div>


	<?php 
	$this->load->view('include/footer');
	?> 						