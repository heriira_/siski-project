<?php 
$this->load->view('include/header');
 ?>
 <!-- Content -->
 <title>Sistem Koperasi - Angsuran</title>
 <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
 	<div class="row">
 		<ol class="breadcrumb">
 			<li><a href="#">
 				<em class="fa fa-sitemap"></em>
 			</a></li>
 			<li class="active">Angsuran</li>
 		</ol>
 	</div><!--/.row-->

 	<div class="row">
 		<div class="col-lg-12">
 			<h2 class="page-header">Angsuran</h2>
 			<div class="form-group text-left">
 				<a href="<?php echo base_url(); ?>Angsuran/Tambah" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Angsuran</a>
 			</div>
 			<?=$this->session->flashdata('notif')?>
 		</div>
 	</div><!--/.row-->
 	<div class="row">
 		<div class="col-md-12">
 			<div class="panel panel-default">
 				<div class="panel-heading">
 					Data Angsuran
 					<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
 					<div class="panel-body">
 						<table id="customers2">
							<thead>
								<tr>
									<th><center>No</th>
									<th><center>ID</th>
									<th><center>Nama</th>
									<th><center>Tanggal Bayar</th>
									<th><center>Tanggal Jatuh Tempo</th>
									<th><center>Angsuran Ke</th>
									<th><center>Nominal Angsuran</th>
									<th><center>Keterangan Angsuran</th>
									<th><center>Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$id = $this->uri->segment('3') + 1;
								if( ! empty($tb_angsuran)){
								  foreach($tb_angsuran as $data){ 
								    echo "<tr>";
								    echo "<td><center>".$id++." </td>";
								    echo "<td><center>".$data['id_pinjaman']."</td>";
								    echo "<td><center>".$data['nama_peminjam']."</td>";
								    echo "<td><center>".$data['tanggal_pembayaran']."</td>";
								    echo "<td><center>".$data['tanggal_jatuh_tempo']."</td>";
								    echo "<td><center>".$data['angsuran_ke']."</td>";
								    echo "<td><center>".$data['besar_angsuran']."</td>";
								    echo "<td><center>".$data['keterangan_angsuran']."</td>";
								    ?>
								    <td>
								    	<center>
								    	<ul class="pull panel-settings panel-button-tab">
											<li class="dropdown"><a class="pull dropdown-toggle" data-toggle="dropdown" href="#">
												<em class="fa fa-cogs" style="margin-left: 12px;"></em>
													</a>
														<ul class="dropdown-menu dropdown-menu-right">
															<li>
																<ul class="dropdown-settings">
																	<li><a href="<?php echo base_url(); ?>Angsuran/Lihat/<?php echo $data['id_angsuran']?>">
																		<em class="fa fa-eye"></em> Lihat
																	</a></li>
																	<li class="divider"></li>
																	<li><a href="<?php echo base_url(); ?>Angsuran/Edit/<?php echo $data['id_angsuran']?>">
																		<em class="fa fa-pencil"></em> Edit
																	</a></li>
																	<li class="divider"></li>
																	<li><a href="<?php echo base_url(); ?>Angsuran/Delete/<?php echo $data['id_angsuran']?>" data-confirm="Apakah anda benar-benar yakin akan menghapus data Ansguran dengan ID <?php echo $data['id_pinjaman'] ?> ?"><em class="fa fa-trash"></em> Hapus</a></li>
																</ul>
															</li>
														</ul>
													</li>
												</ul>
											</td>
											<?php
											echo "</tr>";
										}
									}else{ 
										echo "<tr><td colspan='8'><center>Data kosong.</center></td></tr>";
									}
									?>
								</tbody>
							</table>
						</div>

					</div>
				</div>
			</div><!--/.row-->
			<!-- End Content -->
<?php 
$this->load->view('include/footer');
 ?>
 <script type="text/javascript">
    $(document).ready(function(){
        $('#customers2').DataTable({
        	"ordering": false,
        	"language":{
        		"url":"indonesia.json",
        		"sEmptyTable":"Tidads"
        	}
        });
    });
 </script>