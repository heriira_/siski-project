<?php 
$this->load->view('include/header');
?>

<!-- Content -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-sitemap"></em>
			</a></li>
			<li class="active">Angsuran</li>
			<li class="active">Lihat</li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Angsuran</h2>
		</div>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php foreach($tb_angsuran as $data){ ?> 
						<?php echo "Data lengkap Anggota dengan Nama " . "<strong><i>" . $data->nama_peminjam . "</i></strong>"; ?>
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
						<div class="panel-body">
							<table class="table">
								<tr><td>ID</td><td><?php echo $data->id_angsuran; ?></td></tr>
								<tr><td>ID Pinjaman</td><td><?php echo $data->id_pinjaman; ?></td></tr>
								<tr><td>Nama Peminjam</td><td><?php echo $data->nama_peminjam; ?></td></tr>
								<tr><td>Tanggal Pembayaran</td><td><?php echo $data->tanggal_pembayaran; ?></td></tr>
								<tr><td>Angsuran Ke</td><td><?php echo $data->angsuran_ke; ?></td></tr>
								<tr><td>Tanggal Jatuh Tempo</td><td><?php echo $data->tanggal_jatuh_tempo; ?></td></tr>
								<tr><td>Besar Angsuran</td><td><?php echo $data->besar_angsuran; ?></td></tr>
								<tr><td>Keterangan Angsuran</td><td><?php echo $data->keterangan_angsuran; ?></td></tr>
								<tr><td></td><td><a href="<?php echo site_url('Angsuran') ?>" class="btn btn-default"><em class="fa fa-arrow-circle-left"></em> Kembali</a></td></tr>
							</table>
						</div>
					<?php }?>
				</div>
			</div>
		</div><!--/.row-->
		<!-- End Content -->

		<?php 
		$this->load->view('include/footer');
		?>