<?php
$this->load->view('include/header');
?>
<title>Sistem Koperasi - Password</title>
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-sitemap"></em>
			</a></li>
			<li class="active">Edit</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Password</h2>
			<?php 
if(isset($_GET['pesan'])){
if($_GET['pesan'] == "berhasil"){echo"<div class='alert bg-success' role='alert'><em class='fa fa-lg fa-warning'>&nbsp;</em> <strong>Password Baru Di simpan.</strong> <a class='close pull-right' data-dismiss='alert' aria-label='Close'><em class='fa fa-lg fa-close'></em></a></div>";
		}
	}
?>
		</div>
	</div><!--/.row-->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Edit Password
					<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em>
					</span></div>
					<div class="panel-body">
						<form role="form" method="post" action="<?php echo base_url().'Password/Ganti' ?>">
						<div class="col-md-6">
							<div class="form-group">
								<label for="pass_baru">Masukan Password Baru</label>
								<input type="password" class="form-control" id="pass_baru" name="pass_baru" value="<?= set_value('pass_baru'); ?>" autocomplete="off">
								<strong><?php echo form_error('pass_baru'); ?></strong>
							</div>
							<div class="form-group">
								<label for="ulang_pass">Ulangi Password</label>
								<input type="password" class="form-control" id="ulang_pass" name="ulang_pass" value="<?= set_value('ulang_pass'); ?>" autocomplete="off">
								<strong><?php echo form_error('ulang_pass'); ?></strong>
							</div>
							<button type="submit" name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Simpan
								</button>
								<a href="<?php echo base_url(); ?>Password/Batal/" class="btn btn-warning" data-confirm2="Apakah anda yakin akan membatalkan operasi ini ?"><span class="glyphicon glyphicon-remove"></span> Batal</a>
						</div>
					</form>
					</div>
				</div>
			</div><!--/.row-->
			<!-- End Content -->
			<?php 
			$this->load->view('include/footer');
			?>
			<script type="text/javascript">
				$(document).ready(function(){
					$('#customers2').DataTable({
						"ordering": false,
						"language":{
							"url":"indonesia.json",
							"sEmptyTable":"Tidads"
						}
					});
				});
			</script>	