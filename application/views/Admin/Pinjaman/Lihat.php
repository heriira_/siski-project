<?php 
$this->load->view('include/header');
?>

<!-- Content -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-sitemap"></em>
			</a></li>
			<li class="active">Pinjaman</li>
			<li class="active">Lihat</li>
		</ol>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Pinjaman</h2>
		</div>
	</div><!--/.row-->
	
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php foreach($tb_pinjaman as $data){ ?> 
						<?php echo "Data lengkap Anggota dengan nama " . "<strong><i>" . $data->id_anggota . "</i></strong>"; ?>
						<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
						<div class="panel-body">
							<table class="table">
								<tr><td>Id</td><td><?php echo $data->id_pinjaman; ?></td></tr>
								<tr><td>Nama</td><td><?php echo $data->id_anggota; ?></td></tr>
								<tr><td>Jumlah Pinjam</td><td><?php echo $data->besar_pinjaman; ?></td></tr>
								<tr><td>Tanggal Ajuan Pinjam</td><td><?php echo $data->tanggal_pengajuan_pinjaman; ?></td></tr>
								<tr><td>Tanggal ACC Pinjam</td><td><?php echo $data->tanggal_acc_peminjaman; ?></td></tr>
								<tr><td>Tanggal Pinjam</td><td><?php echo $data->tanggal_pinjaman; ?></td></tr>
								<tr><td>Tanggal Pelunasan</td><td><?php echo $data->tanggal_pelunasan_pinjaman; ?></td></tr>
								<tr><td>Keterangan </td><td><?php echo $data->keterangan_pinjaman; ?></td></tr>
								<tr><td></td><td><a href="<?php echo site_url('Pinjaman') ?>" class="btn btn-default"><em class="fa fa-arrow-circle-left"></em> Kembali</a></td></tr>
							</table>
						</div>
					<?php }?>
				</div>
			</div>
		</div><!--/.row-->
		<!-- End Content -->

		<?php 
		$this->load->view('include/footer');
		?>