<?php 
$this->load->view('include/header');
?>
<!-- Content -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-sitemap"></em>
			</a></li>
			<li class="active">Pinjaman / Edit</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Pinjaman</h2>
			<?=$this->session->flashdata('notif')?>
		</div>
	</div><!--/.row-->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Edit Data Pinjaman
					<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
						<?php foreach($tb_pinjaman as $data){ ?>   
							<form role="form" method="post" action="<?php echo base_url().'Pinjaman/Edit_pinjaman' ?>">
								<div class="col-md-6">
									<input type="hidden" class="form-control" id="id_pinjaman" name="id_pinjaman" value="<?php echo $data->id_pinjaman; ?>" placeholder="Masukkan Nama" autocomplete="off">
									<div class="form-group"><label>Kategori</label>   
									<select name="kategori" class="form-control">    
										<option value="">-Pilih Kategori-</option> 	 	    
										<?php foreach($tb_pinjaman_kategori as $k){ ?>
											<option <?php if($data->id_pinjaman_kategori == $k->nama_pinjaman_kategori){echo "selected='selected'";} ?> value="<?php echo $k->nama_pinjaman_kategori; ?>">
												<?php echo $k->nama_pinjaman_kategori; ?></option>
											<?php } ?>    
										</select>    
										<?php echo form_error('kategori'); ?> 
									</div>
									<div class="form-group"><label>Anggota</label>
									<select name="anggota" class="form-control">    
										<option value="">-Pilih Anggota-</option> 	 	    
										<?php foreach($tb_anggota as $k){ ?>
											<option <?php if($data->id_anggota == $k->nama_anggota){echo "selected='selected'";} ?> value="<?php echo $k->nama_anggota; ?>">
												<?php echo $k->nama_anggota; ?></option>
											<?php } ?>    
										</select>    
										<?php echo form_error('anggota'); ?> 
									</div>
									<div class="form-group">
										<label for="besar">Besar Pinjaman</label>
										<input type="text" class="form-control" id="besar" name="besar" value="<?php echo $data->besar_pinjaman; ?>" placeholder="Nominal Angsuran" autocomplete="off">
										<strong><?php echo form_error('besar'); ?></strong>
									</div>
									<div class="form-group">
										<label for="pengajuan">Tanggal Pengajuan</label>
										<input type="date" class="form-control" id="pengajuan" name="pengajuan" value="<?php echo $data->tanggal_pengajuan_pinjaman; ?>" autocomplete="off">
										<strong><?php echo form_error('pengajuan'); ?></strong>
										<span class="label label-success">Bulan /Hari /Tahun</span>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="acc">Tanggal ACC</label>
										<input type="date" class="form-control" id="acc" name="acc" value="<?php echo $data->tanggal_acc_peminjaman; ?>" autocomplete="off">
										<strong><?php echo form_error('acc'); ?></strong>
										<span class="label label-success">Bulan /Hari /Tahun</span>
									</div>
									<div class="form-group">
										<label for="lunas">Tanggal Pelunasan</label>
										<input type="date" class="form-control" id="lunas" name="lunas" value="<?php echo $data->tanggal_pelunasan_pinjaman; ?>" autocomplete="off">
										<strong><?php echo form_error('lunas'); ?></strong>
										<span class="label label-success">Bulan /Hari /Tahun</span>
									</div>
									<div class="form-group">
										<label for="keterangan">Keterangan Pinjaman</label>
										<textarea class="form-control" id="keterangan" name="keterangan" value="<?php echo $data->keterangan_pinjaman; ?>" placeholder="Keterangan Pinjaman" autocomplete="off"><?php echo $data->keterangan_pinjaman; ?></textarea>
										<strong><?php echo form_error('keterangan'); ?></strong>
									</div>								
									</div>							
									<button type="submit" name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Simpan
									</button>
									<a href="<?php echo base_url(); ?>Anggota/Batal/" class="btn btn-warning" data-confirm2="Apakah anda yakin akan membatalkan operasi ini ?"><span class="glyphicon glyphicon-remove"></span> Batal</a>
								</form>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>


		<?php 
		$this->load->view('include/footer');
		?> 						