<?php 
$this->load->view('include/header');
?>
<!-- Content -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-sitemap"></em>
			</a></li>
			<li class="active">simpanan / Edit</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">Simpanan</h2>
			<?=$this->session->flashdata('notif')?>
		</div>
	</div><!--/.row-->
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					Edit Data Simpanan
					<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span></div>
					<div class="panel-body">
						<?php foreach($tb_simpanan as $data){ ?>   
							<form role="form" method="post" action="<?php echo base_url().'Simpanan/Edit_simpanan' ?>">
								<div class="col-md-6">
									<input type="hidden" class="form-control" id="id_simpanan" name="id_simpanan" value="<?php echo $data->id_simpanan; ?>" placeholder="Masukkan Nama" autocomplete="off">
									<div class="form-group"><label>Nama</label>   
										<select name="anggota" class="form-control" >    
											<option value="">-Pilih ID-</option>    
											<?php foreach($tb_anggota as $k){ ?>
												<option <?php if($data->id_anggota == $k->nama_anggota){echo "selected='selected'";} ?> value="<?php echo $k->nama_anggota; ?>">
													<?php echo $k->nama_anggota; ?></option>
												<?php } ?>    
											</select>    
											<?php echo form_error('anggota'); ?> 
										</div>
										<div class="form-group">
											<label for="simpanan">Nama Simpanan</label>
											<input type="text" class="form-control" id="simpanan" name="simpanan" value="<?php echo $data->nama_simpanan; ?>" autocomplete="off">
											<strong><?php echo form_error('simpanan'); ?></strong>
											<span class="label label-success">Bulan /Hari /Tahun</span>									
										</div>
										<div class="form-group">
											<label for="besar">Nominal Simpanan</label>
											<input type="text" class="form-control" id="besar" name="besar" value="<?php echo $data->besar_simpanan; ?>" autocomplete="off">
											<strong><?php echo form_error('besar'); ?></strong>									
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label for="keterangan">Keterangan Simpanan</label>
											<textarea class="form-control" id="keterangan" name="keterangan" value="<?php echo $data->keterangan_simpanan; ?>" placeholder="Keterangan Simpanan" autocomplete="off"><?php echo $data->keterangan_simpanan; ?></textarea>
											<strong><?php echo form_error('keterangan'); ?></strong></div>
											<button type="submit" name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Simpan
											</button>
											<a href="<?php echo base_url(); ?>Angsuran/Batal/" class="btn btn-warning" data-confirm2="Apakah anda yakin akan membatalkan operasi ini ?"><span class="glyphicon glyphicon-remove"></span> Batal</a>
										</div>
									</form>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>


			<?php 
			$this->load->view('include/footer');
			?> 						