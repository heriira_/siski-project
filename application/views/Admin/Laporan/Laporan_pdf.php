<!DOCTYPE html> 
<html> 
<head>  
	<title></title> 
</head> 
<body>   
	<style type="text/css"> 
  .table-data{ width: 100%; 
   border-collapse: collapse;      
} 

.table-data tr th,   
.table-data tr td{    
 border:1px solid black;    
 font-size: 10pt;   
}    
</style>    

<h3>Laporan Transaksi Pinjaman</h3>

<table>   
	<tr>    
		<td>Dari Tanggal</td>    
		<td>:</td>    
		<td><?php echo date('d/m/Y',strtotime($_GET['dari'])); ?></td>   
	</tr>   
	<tr>    
		<td>Sampai Tanggal</td>    
		<td>:</td>    
		<td><?php echo date('d/m/Y',strtotime($_GET['sampai'])); ?></td>      
  </tr>  
</table>

<br/>  

<table class="table-data">   
	<thead>    
		<tr>
			<th><center>Id</th>
            <th>Kategori Pinjaman</th>
            <th>Anggota</th>
            <th>Rp.Pinjaman/Sisa</th>
            <th>Pengajuan</th>
            <th>ACC</th>
            <th>Tanggal Pinjaman</th>
            <th>Pelunasan</th>
            <th>Keterangan</th>
        </tr>
    </thead>
    <?php $no = 1; foreach($laporan as $m){ ?>
        <tr> 
           
            <td><?php echo $no++; ?></td>
            <td><?php echo $m->id_pinjaman_kategori ?></td>
            <td><?php echo $m->id_anggota ?></td>
            <td><?php echo $m->besar_pinjaman ?></td>
            <td><?php echo $m->tanggal_pengajuan_pinjaman ?></td>
            <td><?php echo $m->tanggal_acc_peminjaman ?></td>
            <td><?php echo $m->tanggal_pinjaman ?></td>
            <td><?php echo $m->tanggal_pelunasan_pinjaman ?></td>
            <td><?php echo $m->keterangan_pinjaman ?></td>
            
        
        <?php 
    }
    ?>
</tbody>
</table>
<script type="text/javascript">  window.print(); </script> 
</body>
</html>           