<?php 
$this->load->view('include/header');
?>
<!-- Content -->
<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
	<div class="row">
		<ol class="breadcrumb">
			<li><a href="#">
				<em class="fa fa-sitemap"></em>
			</a></li>
			<li class="active">Laporan</li>
		</ol>
	</div><!--/.row-->

	<div class="row">
		<div class="col-lg-12">
			<h2 class="page-header">laporan</h2>
			<?=$this->session->flashdata('notif')?>
		</div>
	</div><!--/.row-->

	<div class="panel panel-default">
		<div class="panel-heading">
			Tampilkan Data Laporan
			<span class="pull-right clickable panel-toggle panel-button-tab-left"><em class="fa fa-toggle-up"></em></span>
		</div>
		<div class="panel-body">
			<div class="col-md-6">
				<form role="form" method="post" action="<?php echo base_url().'Laporan' ?>">
					<div class="form-group">
						<label for="dari">Dari Tanggal</label>
						<input type="date" class="form-control" id="dari" name="dari" value="<?php echo set_value('dari'); ?>" placeholder="" autocomplete="off">
						<strong><?php echo form_error('dari'); ?></strong>
					</div>
					<div class="form-group">
						<label for="sampai">Sampai Tanggal</label>
						<input type="date" class="form-control" id="sampai" name="sampai" value="<?php echo set_value('sampai'); ?>" placeholder="Masukkan Kategori" autocomplete="off">
						<strong><?php echo form_error('sampai'); ?></strong>
					</div>
					<button type="submit" name="submit" class="btn btn-primary"><span class="glyphicon glyphicon-save"></span> Cari
					</button>
				<input type="hidden" class="form-control" id="" name="" value="" placeholder="Masukkan Nama" autocomplete="off"></br>
				
				</div></form></div>
				<div class="panel-body">
				<align-right><div class="btn-group">  
<a class="btn btn-warning btn-sm" href="<?php echo base_url().'Laporan/Laporan_pdf/?dari='.set_value('dari').'&sampai='.set_value('sampai') ?>"><span class="glyphicon glyphiconprint"></span> Cetak PDF</a>   
<a class="btn btn-default btn-sm" href="<?php echo base_url().'Laporan/Laporan_print/?dari='.set_value('dari').'&sampai=' .set_value('sampai') ?>"><span class="glyphicon glyphiconprint"></span> Print</a>  </div></center>
						
						<table id="customers2">
							<thead>
								<tr>
									<th><center>Id</th>
									<th><center>Kategori Pinjaman</th>
									<th><center>Anggota</th>
									<th><center>Rp.Pinjaman/Sisa</th>
									<th><center>Pengajuan</th>
									<th><center>ACC</th>
									<th><center>Tanggal Pinjaman</th>
									<th><center>Pelunasan</th>
									<th><center>Keterangan</th>
								</tr>
							</thead>
							<tbody>
								<?php $no = 1; foreach($laporan as $m){ ?>
                                                        <tr> 
                                                           
                                                            <!-- <td></td> -->
                                                            <td><center><?php echo $no++; ?></td>
                                                            <td><center><?php echo $m->id_pinjaman_kategori ?></td>
                                                            <td><center><?php echo $m->id_anggota ?></td>
                                                            <td><center><?php echo $m->besar_pinjaman ?></td>
                                                            <td><center><?php echo $m->tanggal_pengajuan_pinjaman ?></td>
                                                            <td><center><?php echo $m->tanggal_acc_peminjaman ?></td>
                                                            <td><center><?php echo $m->tanggal_pinjaman ?></td>
                                                            <td><center><?php echo $m->tanggal_pelunasan_pinjaman ?></td>
                                                            <td><center><?php echo $m->keterangan_pinjaman ?></td>
                                                        <?php } ?>
								   											<!--  -->
								</tbody>
							</table>
							</div>
</div>
</div>


<?php 
$this->load->view('include/footer');
?>