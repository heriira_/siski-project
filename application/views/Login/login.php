<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Sistem Koperasi - Login</title>
	<link href="<?= base_url() ?>/assets/back/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>/assets/back/css/datepicker3.css" rel="stylesheet">
	<link href="<?= base_url() ?>/assets/back/css/styles.css" rel="stylesheet">
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<div class="row">
		<div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
			<?=$this->session->flashdata('notif')?>
			<?php 
		if(isset($_GET['pesan'])){
			if($_GET['pesan'] == "logout"){
				echo "<div class='alert bg-danger' role='alert'><em class='fa fa-lg fa-check-square'>&nbsp;</em> <strong>Anda telah logout.</strong> <a class='close pull-right' data-dismiss='alert' aria-label='Close'><em class='fa fa-lg fa-close'></em></a> </div>";
			}else if ($_GET['pesan'] == "gagal"){
				echo "<div class='alert bg-danger' role='alert'><em class='fa fa-lg fa-check-square'>&nbsp;</em> <strong>Login gagal, Cek kombinasi Username & Password !</strong> <a class='close pull-right' data-dismiss='alert' aria-label='Close'><em class='fa fa-lg fa-close'></em></a> </div>";
			}else if ($_GET['pesan'] == "belumlogin"){
				echo "<div class='alert bg-warning' role='alert'><em class='fa fa-lg fa-check-square'>&nbsp;</em> <strong>Silahkan Login Dulu !</strong> <a class='close pull-right' data-dismiss='alert' aria-label='Close'><em class='fa fa-lg fa-close'></em></a> </div>";	

		}
	}
		?>
		 			<div class="login-panel panel panel-default">
				<div class="panel-heading">Log in</div>
				<div class="panel-body">
					<form role="form" method="post" action="<?php echo base_url().'Login/login' ?>">
						<fieldset>
							<div class="form-group">
								<input class="form-control" placeholder="E-mail" name="username" type="text" autofocus="">
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="password" type="password" value="">
							</div>
							<div class="checkbox">
								<label>
									<input name="remember" type="checkbox" value="Remember Me">Remember Me
								</label>
							</div>
							<button type="submit" name="submit" class="btn btn-primary"> Login
					</button>
					</form>
				</div>
			</div>
		</div><!-- /.col-->
	</div><!-- /.row -->	
<?php 
	$this->load->view('include/footer')
?>		