<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- <title>Sistem Koperasi - Dashboard</title> -->
	<link href="<?= base_url() ?>/assets/back/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?= base_url() ?>/assets/datatables/css/dataTables.bootstrap.min.css" rel='stylesheet' type='text/css' />
	<link href="<?= base_url() ?>/assets/back/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?= base_url() ?>/assets/back/css/datepicker3.css" rel="stylesheet">
	<link href="<?= base_url() ?>/assets/back/css/styles.css" rel="stylesheet">
	
	<!--Custom Font-->
<!-- 	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet"> -->
	<!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
<![endif]-->
<style>
#customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
   text-align: left;
   padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

/*#customers tr:hover {background-color: #ddd;}*/

#customers th {
    background-color: #30a5ff;
    color: white;
}

#customers2{
	font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%; 
}

#customers2 td, #customers2 th {
   border: 0px solid #ddd;
   padding: 8px;
}

#customers2 tr:nth-child(even){background-color: #f2f2f2;}

#customers2 tr:hover{
	background-color: #D8EEFE;
}

#customers2 th{
	padding-top: 12px;
	padding-bottom: 12px;
	text-align: left;
	background-color: #30a5ff;
	color: white;
}
</style>
</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
					<a class="navbar-brand" href="<?php echo base_url(); ?>Dashboard"><span>Sistem | </span>Koperasi</a>
					<ul class="nav navbar-top-links navbar-right">
						<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
							<em class="fa fa-envelope"></em><span class="label label-danger">1</span>
						</a>
						<ul class="dropdown-menu dropdown-messages">
							<li>
								<div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
									<img alt="image" class="img-circle" src="<?= base_url() ?>/assets/back/img/heri.jpg">
								</a>
								<div class="message-body"><small class="pull-right">3 mins ago</small>
									<a href="#"><strong>Heri Irawan</strong> Assallamualaikum .</a>
									<br /><small class="text-muted">23:20 pm - 05/05/2019</small></div>
								</div>
							</li>
							<li class="divider"></li>
							<li>
								<div class="all-button"><a href="#">
									<em class="fa fa-inbox"></em> <strong>All Messages</strong>
								</a></div>
							</li>
						</ul>
					</li>
					<li class="dropdown"><a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
						<em class="fa fa-lg fa-user"></em>
					</a>
					<ul class="dropdown-menu dropdown-alerts">
						<li><a href="<?= base_url(); ?>Password">
							<div><em class="fa fa-user"></em> Ganti Password
								</div>
							</a>
						</li>
					</ul>
					</li>
						</ul>
					</div>
				</div><!-- /.container-fluid -->
			</nav>
			<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
				<div class="profile-sidebar">
					<div class="profile-userpic">
						<img src="<?= base_url() ?>/assets/back/img/1.png" class="img-responsive" alt="">

					</div>
					<div class="profile-usertitle">
						<div class="profile-usertitle-name">Admin</div>

						<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="divider"></div>
<!-- 				<form role="search">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Search">
					</div>
				</form> -->
				<ul class="nav menu">
					<li><a href="<?php echo base_url(); ?>Dashboard"><em class="fa fa-dashboard">&nbsp;</em> Dashboard</a></li>
					<li><a href="<?php echo base_url(); ?>Anggota"><em class="fa fa-users">&nbsp;</em> Anggota</a></li>
					<li><a href="<?php echo base_url(); ?>Angsuran"><em class="fa fa-bar-chart">&nbsp;</em> Angsuran</a></li>
					<li><a href="<?php echo base_url(); ?>Petugas"><em class="glyphicon glyphicon-user"></em> Petugas</a></li>
					<li><a href="<?php echo base_url(); ?>Pinjaman"><em class="fa fa-book">&nbsp;</em> Pinjaman</a></li>
					<li><a href="<?php echo base_url(); ?>Kategori"><em class="fa fa-clone">&nbsp;</em> Kategori</a></li>
					<li><a href="<?php echo base_url(); ?>Simpanan"><em class="fa fa-bar-chart">&nbsp;</em> Simpanan</a></li>
					<li><a href="<?php echo base_url(); ?>Laporan"><em class="fa fa-calendar">&nbsp;</em> Laporan</a></li>

					<!-- <li class="parent "><a data-toggle="collapse" href="#sub-item-1">
						<em class="fa fa-navicon">&nbsp;</em> Multilevel <span data-toggle="collapse" href="#sub-item-1" class="icon pull-right"><em class="fa fa-plus"></em></span>
					</a>
					<ul class="children collapse" id="sub-item-1">
						<li><a class="" href="#">
							<span class="fa fa-arrow-right">&nbsp;</span> Sub Item 1
						</a></li>
						<li><a class="" href="#">
							<span class="fa fa-arrow-right">&nbsp;</span> Sub Item 2
						</a></li>
						<li><a class="" href="#">
							<span class="fa fa-arrow-right">&nbsp;</span> Sub Item 3
						</a></li>
					</ul>
				</li> -->
				<li><a href="<?php echo base_url('Login/logout'); ?>"." data-confirm="Apakah anda yakin akan keluar dari halaman admin ?"><em class="fa fa-power-off">&nbsp;</em> Logout</a></li>
			</ul>
		</div><!--/.sidebar-->