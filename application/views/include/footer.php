<div class="col-sm-12">
				<p class="back-link">Develover by <a href="https://www.instagram.com/heriira_/?hl=id" target="_blank">Heri Irawan</a></p>
			</div>
		</div><!--/.row-->
	</div>	<!--/.main-->
<script src="<?= base_url() ?>/assets/back/js/jquery-1.11.1.min.js"></script>
<script src="<?= base_url() ?>/assets/back/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>/assets/back/js/chart.min.js"></script>
<script src="<?= base_url() ?>/assets/back/js/chart-data.js"></script>
<script src="<?= base_url() ?>/assets/back/js/easypiechart.js"></script>
<script src="<?= base_url() ?>/assets/back/js/easypiechart-data.js"></script>
<script src="<?= base_url() ?>/assets/back/js/bootstrap-datepicker.js"></script>
<script src="<?= base_url() ?>/assets/back/js/custom.js"></script>
<script src="<?= base_url() ?>/assets/datatables/js/jquery.js"></script>
<script src="<?= base_url() ?>/assets/datatables/js/jquery.dataTables.js"></script>
<script src="<?= base_url() ?>/assets/datatables/js/dataTables.bootstrap.min.js"></script>
<script>
	window.onload = function () {
		var chart1 = document.getElementById("line-chart").getContext("2d");
		window.myLine = new Chart(chart1).Line(lineChartData, {
			responsive: true,
			scaleLineColor: "rgba(0,0,0,.2)",
			scaleGridLineColor: "rgba(0,0,0,.05)",
			scaleFontColor: "#c5c7cc"
		});
	};
</script>
   <script>
    $(document).ready(function() {
		
	 $('a[data-confirm]').click(function(ev) {
		  var href = $(this).attr('href');

		  if(!$('#dataConfirmModal').length) {
		   $('body').append('<div id="dataConfirmModal" class="modal fade bs-modal-sm" tableindex="-1" role="dialog" aria-labelledby="dataConfirmLabel" aria-hiden="true"><div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><h4 class="modal-title" id="dataConfrimLabel">Konfirmasi</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button></div><div class="modal-body"></div><div class="modal-footer"><a class="btn btn-danger btn-sx" aria-hiden="true" id="dataConfirmOK"> Ya, saya yakin </a><button type="button" class="btn btn-default btn-sx" data-dismiss="modal" aria-hiden=""true"> Batal </button></div></div></div></div>');
		   }

		  $('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));

		  $('#dataConfirmOK').attr('href',href);

		  $('#dataConfirmModal').modal({show:true});
		  return false;
		 });
		
	});
    </script>

    <script>
    $(document).ready(function() {
		
	 $('a[data-confirm2]').click(function(ev) {
		  var href = $(this).attr('href');

		  if(!$('#dataConfirmModal').length) {
		   $('body').append('<div id="dataConfirmModal" class="modal fade bs-modal-sm" tableindex="-1" role="dialog" aria-labelledby="dataConfirmLabel" aria-hiden="true"><div class="modal-dialog modal-sm"><div class="modal-content"><div class="modal-header"><h4 class="modal-title" id="dataConfrimLabel">Konfirmasi</h4><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button></div><div class="modal-body"></div><div class="modal-footer"><a class="btn btn-warning btn-sx" aria-hiden="true" id="dataConfirmOK"> Ya, saya yakin </a><button type="button" class="btn btn-default btn-sx" data-dismiss="modal" aria-hiden=""true"> Batal </button></div></div></div></div>');
		   }

		  $('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm2'));

		  $('#dataConfirmOK').attr('href',href);

		  $('#dataConfirmModal').modal({show:true});
		  return false;
		 });
		
	});
    </script>	
</body>
</html>