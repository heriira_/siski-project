<?php 
// Model yang terstruktur. agar bisa digunakan berulang kali untuk membuat CRUD. 
// Sehingga proses pembuatan CRUD menjadi lebih cepat dan efisien.
class M_anggota extends CI_Model{
	
	public $table = 'tb_anggota';
    public $id_anggota = 'id_anggota';

	function edit_data($where,$table){
		return $this->db->get_where($table,$where);
	}
	    function get_by_id($id)
    {
        $this->db->where($this->id_anggota, $id_anggota);
        return $this->db->get($this->table)->row();
    }	
	function get_data($table){
		return $this->db->get($table);
	}
		function get_anggota(){
        $query = $this->db->get('tb_anggota');
        return $query->result_array();
    }
	function insert_data($data,$table){
		$this->db->insert($table,$data);
	}
	function update_data($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	function delete_data($where,$table){
		$this->db->where($where);
		$this->db->delete($table);
	}
	function cek_login($table,$where){
		return $this->db->get_where($table,$where);
	}
}
?>