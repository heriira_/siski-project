<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Simpanan extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->model('M_simpanan');
		$this->form_validation->set_error_delimiters('<div style="color:#f9243f; margin-bottom: 5px; margin-top:2px">', '</div>');


//cek login
		if($this->session->userdata('status') != "admin_login"){ redirect(base_url().'login?pesan=belumlogin');
	}
}
	public function index()
	{

		$data=array('tb_simpanan' => $this->M_simpanan->get_simpanan());
		$this->load->view('Admin/Simpanan/simpanan', $data);

	}
	function Batal()
	{
		$this->session->set_flashdata('notif','<div class="alert bg-warning" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Otorasi dibatalkan.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');
		redirect('Simpanan');
	}

	function Tambah()
	{
		$data['tb_simpanan'] = $this->M_simpanan->get_data('tb_simpanan')->result();
		$data['tb_anggota'] = $this->M_simpanan->get_data('tb_anggota')->result();
		$this->load->view('Admin/Simpanan/Tambah',$data);
	}

	function Tambah_simpanan()
	{
		$anggota = $this->input->post('anggota');
		$simpanan = $this->input->post('simpanan');
		$besar = $this->input->post('besar');
		$keterangan = $this->input->post('keterangan');

		$this->form_validation->set_rules('anggota','Anggota','required'); 
		$this->form_validation->set_rules('simpanan','Simpanan','required'); 
		$this->form_validation->set_rules('besar','Besar','required');
		$this->form_validation->set_rules('keterangan','Keterangan','required');
		if($this->form_validation->run() != false){
			$data = array(  
				'id_anggota' => $anggota,
				'nama_simpanan' => $simpanan,
				'besar_simpanan' => $besar,
				'keterangan_simpanan' => $keterangan,
				'tanggal_simpanan' => date('Y-m-d')	
			);
			$this->M_simpanan->insert_data($data,'tb_simpanan');
			$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil disimpan.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');								 
			redirect(base_url().'Simpanan');  
		}else{   	
			$this->load->view('Admin/Simpanan/Tambah');   
		}
	}

	function Edit($id)
	{
		$where = array( 'id_simpanan' => $id );
		$data['tb_simpanan'] = $this->M_simpanan->edit_data($where,'tb_simpanan')->result();
		$data['tb_anggota'] = $this->M_simpanan->get_data('tb_anggota')->result();
		$this->load->view('Admin/Simpanan/Edit',$data);
	}

	function Edit_simpanan()
	{
		$id_simpanan = $this->input->post('id_simpanan');	
		$anggota = $this->input->post('anggota');
		$simpanan = $this->input->post('simpanan');
		$besar = $this->input->post('besar');
		$keterangan = $this->input->post('keterangan');

		$this->form_validation->set_rules('anggota','Anggota','required'); 
		$this->form_validation->set_rules('simpanan','Simpanan','required'); 
		$this->form_validation->set_rules('besar','Besar','required');
		$this->form_validation->set_rules('keterangan','Keterangan','required'); 	
		if($this->form_validation->run() != false){
			$where = array( 'id_simpanan' => $id_simpanan );
			$data = array(  
				'id_anggota' => $anggota,
				'nama_simpanan' => $simpanan,
				'besar_simpanan' => $besar,
				'keterangan_simpanan' => $keterangan
			);
			$this->M_simpanan->update_data($where,$data,'tb_simpanan');
			$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil diubah.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
			redirect(base_url().'Simpanan');  
		}else{
			$where = array( 'id_simpanan' => $id_simpanan);   
			$data['tb_anggota'] = $this->M_simpanan->get_data('tb_anggota')->result(); 
			$data['tb_simpanan'] = $this->M_simpanan->edit_data($where,'tb_simpanan')->result();     
			$this->load->view('Admin/Simpanan/Edit',$data);
		}
	}

	function Delete($id)
	{
		$where = array('id_simpanan' => $id);
		$this->M_simpanan->delete_data($where,'tb_simpanan');
		$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil dihapus.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
		redirect(base_url().'Simpanan');
	}

	function Lihat($id)
	{
		$where = array( 'id_simpanan' => $id );
		$data['tb_simpanan'] = $this->M_simpanan->edit_data($where,'tb_simpanan')->result();
		$this->load->view('Admin/Simpanan/Lihat',$data);
	}
}
