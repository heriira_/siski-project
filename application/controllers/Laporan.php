<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->model('M_pinjaman');
		$this->form_validation->set_error_delimiters('<div style="color:#f9243f; margin-bottom: 5px; margin-top:2px">', '</div>');


//cek login
		if($this->session->userdata('status') != "admin_login"){ redirect(base_url().'login?pesan=belumlogin');
	}
}
	
function index()
{
$data=array('tb_pinjaman' => $this->M_pinjaman->get_pinjaman());
	$dari = $this->input->post('dari');  
	$sampai = $this->input->post('sampai');    
	$this->form_validation->set_rules('dari','Dari Tanggal','required');    
	$this->form_validation->set_rules('sampai','Sampai Tanggal','required');

	if($this->form_validation->run() != false){    
		
		$data['laporan'] = $this->db->query("select * from tb_pinjaman where date(tanggal_pinjaman) >= '$dari' ")->result();

		$this->load->view('Admin/Laporan/laporan_filter',$data); 

	}else{   
		$this->load->view('Admin/Laporan/laporan');    
	}   

}
 function Laporan_print(){       
 	$dari = $this->input->get('dari');  
 	$sampai = $this->input->get('sampai');       
 	if($dari != "" && $sampai != ""){        
 		$data['laporan'] = $this->db->query("select * from tb_pinjaman where date(tanggal_pinjaman) >= '$dari' ")->result();              
 		$this->load->view('Admin/Laporan/Laporan_print',$data);      
 	}else{   
 		redirect("admin/laporan");   
 	}	   
 }

 function Laporan_pdf(){  
 	$this->load->library('dompdf_gen');   
 	$dari = $this->input->get('dari');  
 	$sampai = $this->input->get('sampai');        
 	$data['laporan'] = $this->db->query("select * from tb_pinjaman where date(tanggal_pinjaman) >= '$dari' ")->result();           
 	$this->load->view('Admin/Laporan/Laporan_pdf', $data);

// ukuran kertas	 
 	$paper_size  = 'A4';
//tipe format kertas potrait atau landscape 
 	$orientation = 'landscape';   
 	$html = $this->output->get_output(); 
//Convert to PDF 
 	$this->dompdf->set_paper($paper_size, $orientation);    
 	$this->dompdf->load_html($html);  
 	$this->dompdf->render();  
// nama file pdf yang di hasilkan 
 	$this->dompdf->stream("laporan.pdf", array('Attachment'=>0)); 

 }
}
