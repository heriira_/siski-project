<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_anggota');
		$this->load->model('M_petugas');

		$this->form_validation->set_error_delimiters('<div style="color:#f9243f; margin-bottom: 5px; margin-top:2px">', '</div>');


//cek login
		if($this->session->userdata('status') != "admin_login"){ redirect(base_url().'login?pesan=belumlogin');
	}
	}

	public function index()
	{
		$data['tb_anggota'] = $this->M_anggota->get_data('tb_anggota')->result();
		$this->load->view('Admin/dashboard',$data);

	}
}
