<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Angsuran extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->model('M_angsuran');
		$this->form_validation->set_error_delimiters('<div style="color:#f9243f; margin-bottom: 5px; margin-top:2px">', '</div>');


//cek login
		if($this->session->userdata('status') != "admin_login"){ redirect(base_url().'login?pesan=belumlogin');
	}
}
	public function index()
	{

		$data=array('tb_angsuran' => $this->M_angsuran->get_angsuran());
		$data['tb_pinjaman'] = $this->M_angsuran->get_data('tb_pinjaman')->result();
		$data['tb_pinjaman_kategori'] = $this->M_angsuran->get_data('tb_pinjaman_kategori')->result(); 			
		$this->load->view('Admin/Angsuran/angsuran', $data);

	}
	function Batal()
	{
		$this->session->set_flashdata('notif','<div class="alert bg-warning" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Otorasi dibatalkan.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
		redirect('Anggota');
	}

	function Tambah()
	{
		$data['tb_angsuran'] = $this->M_angsuran->get_data('tb_angsuran')->result();
		$data['tb_pinjaman'] = $this->M_angsuran->get_data('tb_pinjaman')->result();
		$data['tb_pinjaman_kategori'] = $this->M_angsuran->get_data('tb_pinjaman_kategori')->result();
		$this->load->view('Admin/Angsuran/Tambah',$data);
	}

	function Tambah_angsuran()
	{
		$pinjaman = $this->input->post('pinjaman');
		$anggota = $this->input->post('anggota');
		$tanggal = $this->input->post('tanggal');
		$angsuran = $this->input->post('angsuran');
		$besar = $this->input->post('besar');
		$tempo = $this->input->post('tempo');
		$keterangan = $this->input->post('keterangan');

		$this->form_validation->set_rules('pinjaman','Pinjaman','required'); 
		$this->form_validation->set_rules('anggota','Anggota','required'); 
		$this->form_validation->set_rules('tanggal','Tanggal','required'); 
		$this->form_validation->set_rules('angsuran','Angsuran','required'); 
		$this->form_validation->set_rules('besar','Besar','required');  
		$this->form_validation->set_rules('tempo','Tempo','required');  
		$this->form_validation->set_rules('keterangan','Keterangan','required');
		
		if($this->form_validation->run() != false){
			$data = array(  
				'id_pinjaman' => $pinjaman,
				'nama_peminjam' => $anggota,
				'tanggal_pembayaran' => $tanggal,
				'angsuran_ke' => $angsuran,
				'besar_angsuran' => $besar,
				'tanggal_jatuh_tempo' => $tempo,
				'keterangan_angsuran' => $keterangan
			);
			$cek_uang = $this->M_angsuran->cek_uang($besar);
		foreach ($cek_uang as $key) {
			if ($key->besar_angsuran < $besar_pinjaman) {
				$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil disimpan.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');				
			}
		}
			$this->M_angsuran->insert_data($data,'tb_angsuran');
			$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil disimpan.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');							 
			redirect(base_url().'Angsuran');  
		}else{ 
			$data['tb_angsuran'] = $this->M_angsuran->get_data('tb_angsuran')->result();
			$data['tb_pinjaman'] = $this->M_angsuran->get_data('tb_pinjaman')->result();
			$data['tb_pinjaman_kategori'] = $this->M_angsuran->get_data('tb_pinjaman_kategori')->result();
			$this->load->view('Admin/Angsuran/Tambah',$data);   
		}
	}

	function Edit($id)
	{
		$where = array( 'id_angsuran' => $id );
		$data['tb_angsuran'] = $this->M_angsuran->edit_data($where,'tb_angsuran')->result();
		$data['tb_pinjaman'] = $this->M_angsuran->get_data('tb_pinjaman')->result();
		$data['tb_pinjaman_kategori'] = $this->M_angsuran->get_data('tb_pinjaman_kategori')->result();
		$this->load->view('Admin/Angsuran/Edit',$data);
	}

	function Edit_angsuran()
	{
		$id_angsuran = $this->input->post('id_angsuran');	
		$anggota = $this->input->post('anggota');	
		$pinjaman = $this->input->post('pinjaman');
		$tanggal = $this->input->post('tanggal');
		$angsuran = $this->input->post('angsuran');
		$tempo = $this->input->post('tempo');
		$besar = $this->input->post('besar');
		$keterangan = $this->input->post('keterangan');

		$this->form_validation->set_rules('pinjaman','Pinjaman','required'); 
		$this->form_validation->set_rules('anggota','Anggota','required'); 
		$this->form_validation->set_rules('tanggal','Tanggal','required'); 
		$this->form_validation->set_rules('angsuran','Angsuran','required'); 
		$this->form_validation->set_rules('besar','Besar','required');  
		$this->form_validation->set_rules('tempo','Tempo','required');  
		$this->form_validation->set_rules('keterangan','Keterangan','required'); 	
		if($this->form_validation->run() != false){
			$where = array( 'id_angsuran' => $id_angsuran );
			$data = array(  
				'id_pinjaman' => $pinjaman,
				'nama_peminjam' => $anggota,
				'tanggal_pembayaran' => $tanggal,
				'angsuran_ke' => $angsuran,
				'besar_angsuran' => $besar,
				'tanggal_jatuh_tempo' => $tempo,
				'keterangan_angsuran' => $keterangan
			);
			$this->M_angsuran->update_data($where,$data,'tb_angsuran');
			$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil diubah.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
			redirect(base_url().'Angsuran');  
		}else{
			$where = array( 'id_angsuran' => $id_angsuran);   
			$data['tb_angsuran'] = $this->M_angsuran->edit_data($where,'tb_angsuran')->result();
			$data['tb_pinjaman'] = $this->M_angsuran->get_data('tb_pinjaman')->result();
			$data['tb_pinjaman_kategori'] = $this->M_angsuran->get_data('tb_pinjaman_kategori')->result();     
			$this->load->view('Admin/Angsuran/Edit',$data);
		}
	}

	function Delete($id)
	{
		$where = array('id_angsuran' => $id);
		$this->M_angsuran->delete_data($where,'tb_angsuran');
		$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil dihapus.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
		redirect(base_url().'Angsuran');
	}

	function Lihat($id)
	{
		$where = array( 'id_angsuran' => $id );
		$data['tb_angsuran'] = $this->M_angsuran->edit_data($where,'tb_angsuran')->result();
		$this->load->view('Admin/Angsuran/Lihat',$data);
	}
}
