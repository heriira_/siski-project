<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_anggota');
		$this->form_validation->set_error_delimiters('<div style="color:#f9243f; margin-bottom: 5px; margin-top:2px">', '</div>');


//cek login
		if($this->session->userdata('status') != "admin_login"){ redirect(base_url().'login?pesan=belumlogin');
	}
	}

	public function index()
	{
		$data=array(
			'tb_anggota' => $this->M_anggota->get_anggota()
		);
		$this->load->view('Admin/Anggota/anggota', $data);

	}

	function Batal()
	{
		$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Otorasi dibatalkan.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
		redirect('Anggota');
	}

	function Tambah()
	{
		$data['tb_anggota'] = $this->M_anggota->get_data('tb_anggota')->result();
		$this->load->view('Admin/Anggota/Tambah',$data);
	}

	function Tambah_anggota()
	{
		$nama = $this->input->post('nama');
		$no_telp = $this->input->post('no_telp');
		$tempat = $this->input->post('tempat');
		$status = $this->input->post('status');
		$tanggal = $this->input->post('tanggal');
		$alamat = $this->input->post('alamat');
		$jk = $this->input->post('jk');
		$keterangan = $this->input->post('keterangan');

		$this->form_validation->set_rules('nama','Nama','required'); 
		$this->form_validation->set_rules('no_telp','No Telp','required'); 
		$this->form_validation->set_rules('tempat','Tempat','required'); 
		$this->form_validation->set_rules('status','Status','required'); 
		$this->form_validation->set_rules('tanggal','Tanggal','required'); 
		$this->form_validation->set_rules('alamat','Alamat','required'); 
		$this->form_validation->set_rules('jk','Jenis Kelamin','required'); 
		$this->form_validation->set_rules('keterangan','Keterangan','required'); 
		if($this->form_validation->run() != false){
			$data = array(  
				'nama_anggota' => $nama,
				'no_telp_anggota' => $no_telp,
				'tempat_lahir_anggota' => $tempat,
				'status_anggota' => $status,
				'tanggal_lahir_anggota' => $tanggal,
				'alamat_anggota' => $alamat,
				'jenis_kelamin_anggota' => $jk,
				'keterangan_anggota' => $keterangan
			);
			$this->M_anggota->insert_data($data,'tb_anggota');
			$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil disimpan.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');									 
			redirect(base_url().'Anggota');  
		}else{   	
			$this->load->view('Admin/Anggota/Tambah');   
		}
	}

	function Edit($id)
	{
		$where = array( 'id_anggota' => $id );
		$data['tb_anggota'] = $this->M_anggota->edit_data($where,'tb_anggota')->result();
		$this->load->view('Admin/Anggota/Edit',$data);
	}

	function Edit_anggota()
	{
		$id_anggota = $this->input->post('id_anggota');	
		$nama = $this->input->post('nama');
		$no_telp = $this->input->post('no_telp');
		$tempat = $this->input->post('tempat');
		$status = $this->input->post('status');
		$tanggal = $this->input->post('tanggal');
		$alamat = $this->input->post('alamat');
		$jk = $this->input->post('jk');
		$keterangan = $this->input->post('keterangan');

		$this->form_validation->set_rules('nama','Nama','required'); 
		$this->form_validation->set_rules('no_telp','No Telp','required'); 
		$this->form_validation->set_rules('tempat','Tempat','required'); 
		$this->form_validation->set_rules('status','Status','required'); 
		$this->form_validation->set_rules('tanggal','Tanggal','required'); 
		$this->form_validation->set_rules('alamat','Alamat','required'); 
		$this->form_validation->set_rules('jk','Jenis Kelamin','required'); 
		$this->form_validation->set_rules('keterangan','Keterangan','required'); 	
		if($this->form_validation->run() != false){
			$where = array( 'id_anggota' => $id_anggota );
			$data = array(  
				'nama_anggota' => $nama,
				'no_telp_anggota' => $no_telp,
				'tempat_lahir_anggota' => $tempat,
				'status_anggota' => $status,
				'tanggal_lahir_anggota' => $tanggal,
				'alamat_anggota' => $alamat,
				'jenis_kelamin_anggota' => $jk,
				'keterangan_anggota' => $keterangan
			);
			$this->M_anggota->update_data($where,$data,'tb_anggota');
			$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil di ubah.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
			redirect(base_url().'Anggota');  
		}else{
			$where = array( 'id_anggota' => $id_anggota);   
			$data['tb_anggota'] = $this->M_anggota->edit_data($where,'tb_anggota')->result();     
			$this->load->view('Admin/Anggota/Edit',$data);
		}
	}

	function Delete($id)
	{
		$where = array('id_anggota' => $id);
		$this->M_anggota->delete_data($where,'tb_anggota');
		$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil dihapus.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
		redirect(base_url().'Anggota');
	}

	// function Lihat($id){
	// 	$row = $this->M_anggota->get_by_id($id_anggota);
	// 	if ($row) {
	// 		$data = array(
	// 			'id_anggota'             => $row->id_anggota,
	// 			'nama_kategori'   => $row->nama_kategori,
	// 		);
	// 		$data['tb_anggota'] = $this->M_anggota->get_data('tb_anggota');
	// $this->load->view('Admin/Anggota/Lihat', $data);
	// 	} else {
	// 		$this->session->set_flashdata('message', 'Record Not Found');
	// 		redirect(site_url('Anggota'));
	// 	}
	// }
	function Lihat($id)
	{
		$where = array( 'id_anggota' => $id );
		$data['tb_anggota'] = $this->M_anggota->edit_data($where,'tb_anggota')->result();
		$this->load->view('Admin/Anggota/Lihat',$data);
	}
}