<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pinjaman extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->model('M_pinjaman');
		$this->form_validation->set_error_delimiters('<div style="color:#f9243f; margin-bottom: 5px; margin-top:2px">', '</div>');


//cek login
		if($this->session->userdata('status') != "admin_login"){ redirect(base_url().'login?pesan=belumlogin');
	}
}
	public function index()
	{

		$data=array('tb_pinjaman' => $this->M_pinjaman->get_pinjaman());
		$this->load->view('Admin/Pinjaman/pinjaman', $data);

	}
	function Batal()
	{
		$this->session->set_flashdata('notif','<div class="alert bg-warning" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Otorasi dibatalkan.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
		redirect('Pinjaman');
	}

	function Tambah()
	{
		$data['tb_pinjaman'] = $this->M_pinjaman->get_data('tb_pinjaman')->result();
		$data['tb_anggota'] = $this->M_pinjaman->get_data('tb_anggota')->result();
		$data['tb_pinjaman_kategori'] = $this->M_pinjaman->get_data('tb_pinjaman_kategori')->result();
		$this->load->view('Admin/Pinjaman/Tambah',$data);
	}

	function Tambah_pinjaman()
	{
		$kategori = $this->input->post('kategori');
		$anggota = $this->input->post('anggota');
		$besar = $this->input->post('besar');
		$pengajuan = $this->input->post('pengajuan');
		$acc = $this->input->post('acc');
		$lunas = $this->input->post('lunas');
		$keterangan = $this->input->post('keterangan');
		
		$this->form_validation->set_rules('kategori','Kategori','required');
		$this->form_validation->set_rules('anggota','Anggota','required');
		$this->form_validation->set_rules('besar','Besar','required');
		$this->form_validation->set_rules('pengajuan','Pengajuan','required');
		$this->form_validation->set_rules('acc','Acc','required');
		$this->form_validation->set_rules('lunas','Lunas','required');
		$this->form_validation->set_rules('keterangan','Keterangan','required');
		if($this->form_validation->run() != false){
			$data = array(  
				'id_pinjaman_kategori' => $kategori,
				'id_anggota' => $anggota,
				'besar_pinjaman' => $besar,
				'tanggal_pengajuan_pinjaman' => $pengajuan,
				'tanggal_acc_peminjaman' => $acc,
				'tanggal_pelunasan_pinjaman' => $lunas,
				'keterangan_pinjaman' => $keterangan,
				'tanggal_pinjaman' => date('Y-m-d')	
			);
		$query = $this->db->query("SELECT * FROM tb_pinjaman WHERE id_pinjaman = '$id_pinjaman'")->result_array();
		foreach ( $query as $q ) {
			$data2 = array(
				'besar_pinjaman'            => $q['besar_angsuran'] + $besar_angsuran
			);
			$w2 = array(
				'id_pinjaman' => $id_pinjaman
			);
		}	
			$this->M_pinjaman->insert_data($data,'tb_pinjaman');
			$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil disimpan.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');							 
			redirect(base_url().'Pinjaman');  
		}else{ 
			$data['tb_pinjaman'] = $this->M_pinjaman->get_data('tb_pinjaman')->result();
			$data['tb_anggota'] = $this->M_pinjaman->get_data('tb_anggota')->result();
			$data['tb_pinjaman_kategori'] = $this->M_pinjaman->get_data('tb_pinjaman_kategori')->result();  	
			$this->load->view('Admin/Pinjaman/Tambah',$data);   
		}
	}

	function Edit($id)
	{
		$where = array( 'id_pinjaman' => $id );
		$data['tb_pinjaman'] = $this->M_pinjaman->edit_data($where,'tb_pinjaman')->result();
		$data['tb_pinjaman_kategori'] = $this->M_pinjaman->get_data('tb_pinjaman_kategori')->result();
		$data['tb_anggota'] = $this->M_pinjaman->get_data('tb_anggota')->result();
		$this->load->view('Admin/Pinjaman/Edit',$data);
	}

	function Edit_pinjaman()
	{
		$id_pinjaman = $this->input->post('id_pinjaman');	
		$kategori = $this->input->post('kategori');
		$anggota = $this->input->post('anggota');
		$besar = $this->input->post('besar');
		$pengajuan = $this->input->post('pengajuan');
		$acc = $this->input->post('acc');
		$lunas = $this->input->post('lunas');
		$keterangan = $this->input->post('keterangan');
		
		$this->form_validation->set_rules('kategori','kategori','required'); 
		$this->form_validation->set_rules('anggota','anggota','required'); 
		$this->form_validation->set_rules('besar','besar','required'); 
		$this->form_validation->set_rules('pengajuan','pengajuan','required'); 
		$this->form_validation->set_rules('acc','acc','required'); 
		$this->form_validation->set_rules('lunas','lunas','required'); 
		$this->form_validation->set_rules('keterangan','keterangan','required'); 
		if($this->form_validation->run() != false){
			$where = array( 'id_pinjaman' => $id_pinjaman );
			$data = array(  
				'id_pinjaman_kategori' => $kategori,
				'id_anggota' => $anggota,
				'besar_pinjaman' => $besar,
				'tanggal_pengajuan_pinjaman' => $pengajuan,
				'tanggal_acc_peminjaman' => $acc,
				'tanggal_pelunasan_pinjaman' => $lunas,
				'keterangan_pinjaman' => $keterangan,
				'tanggal_pinjaman' => date('Y-m-d')
				);
			$this->M_pinjaman->update_data($where,$data,'tb_pinjaman');
			$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil diubah.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
			redirect(base_url().'Pinjaman');  
		}else{
			$where = array( 'id_pinjaman' => $id_pinjaman);
			$data['tb_pinjaman'] = $this->M_pinjaman->edit_data($where,'tb_pinjaman')->result();
		$data['tb_pinjaman_kategori'] = $this->M_pinjaman->get_data('tb_pinjaman_kategori')->result();
		$data['tb_anggota'] = $this->M_pinjaman->get_data('tb_anggota')->result();   
			$data['tb_pinjaman'] = $this->M_pinjaman->edit_data($where,'tb_pinjaman')->result();     
			$this->load->view('Admin/Pinjaman/Edit',$data);
		}
	}

	function Delete($id)
	{
		$where = array('id_pinjaman' => $id);
		$this->M_pinjaman->delete_data($where,'tb_pinjaman');
		$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil dihapus.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
		redirect(base_url().'Pinjaman');
	}

	function Lihat($id)
	{
		$where = array( 'id_pinjaman' => $id );
		$data['tb_pinjaman'] = $this->M_pinjaman->edit_data($where,'tb_pinjaman')->result();
		$this->load->view('Admin/Pinjaman/Lihat',$data);
	}
}
