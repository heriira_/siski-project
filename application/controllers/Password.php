<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Password extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->model('M_pinjaman');
		$this->form_validation->set_error_delimiters('<div style="color:#f9243f; margin-bottom: 5px; margin-top:2px">', '</div>');


//cek login
		if($this->session->userdata('status') != "admin_login"){ redirect(base_url().'login?pesan=belumlogin');
	}
}
	
function index()
{
	$this->load->view('Admin/Password/password');
}

function Ganti(){
	$pass_baru = $this->input->post('pass_baru');
	$ulang_pass = $this->input->post('ulang_pass');

	$this->form_validation->set_rules('pass_baru','Password Baru','required|matches[ulang_pass]');
	$this->form_validation->set_rules('ulang_pass','Ulangi Password Baru','required');

	if($this->form_validation->run() != false){
		$data = array('password' => md5($pass_baru)
	);
		$w = array('id_user' => $this->session->userdata('id')
	);
		$this->M_pinjaman->update_data($w,$data,'tb_user');
		redirect(base_url().'Password?pesan=berhasil');
	}else{
		$this->load->view('Admin/Password/password');

}		
}

	function Batal()
	{
		$this->session->set_flashdata('notif','<div class="alert bg-warning" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Otorasi dibatalkan.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
		redirect('Dashboard');
	}

}
