<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->model('M_login');
		$this->load->library('form_validation');
		$this->form_validation->set_error_delimiters('<div style="color:#f9243f; margin-bottom: 5px; margin-top:2px">', '</div>');
	}

	function index(){
		$this->load->view('Login/login');
	}
	function login(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$this->form_validation->set_rules('username','Username','trim|required');
		$this->form_validation->set_rules('password','Password','trim|required');
		if($this->form_validation->run() != false){
			$where = array(
				'username' => $username,
				'password' => md5($password)
			);
			$data = $this->M_login->edit_data($where,'tb_user');
			$d = $this->M_login->edit_data($where,'tb_user')->row();
			$cek = $data->num_rows();
			if($cek > 0){
				$session = array(	'id'=> $d->id_user,
					'nama'=> $d->nama_petugas,
					'status' => 'admin_login');
				$this->session->set_userdata($session);
				redirect(base_url().'Dashboard');
			}else{
				redirect(base_url().'Login?pesan=gagal');
			}
		}else{$this->load->view('Login/login');
	}
}


function logout(){  $this->session->sess_destroy();  redirect(base_url().'Login?pesan=logout'); }

}