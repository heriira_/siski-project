<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Petugas extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->model('M_petugas');
		$this->form_validation->set_error_delimiters('<div style="color:#f9243f; margin-bottom: 5px; margin-top:2px">', '</div>');


//cek login
		if($this->session->userdata('status') != "admin_login"){ redirect(base_url().'login?pesan=belumlogin');
	}
}
	public function index()
	{

		$data=array('tb_petugas' => $this->M_petugas->get_petugas());
		$this->load->view('Admin/Petugas/petugas', $data);

	}
	function Batal()
	{
		$this->session->set_flashdata('notif','<div class="alert bg-warning" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Otorasi dibatalkan.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
		redirect('Anggota');
	}

	function Tambah()
	{
		$data['tb_petugas'] = $this->M_petugas->get_data('tb_petugas')->result();
		$this->load->view('Admin/Petugas/Tambah',$data);
	}

	function Tambah_petugas()
	{
		$nama = $this->input->post('nama');
		$tempat = $this->input->post('tempat');
		$tanggal = $this->input->post('tanggal');
		$no_telp = $this->input->post('no_telp');
		$alamat = $this->input->post('alamat');
		$keterangan = $this->input->post('keterangan');

		$this->form_validation->set_rules('nama','Nama','required'); 
		$this->form_validation->set_rules('tempat','Tempat','required'); 
		$this->form_validation->set_rules('tanggal','Tanggal','required'); 
		$this->form_validation->set_rules('no_telp','No Telp','required'); 
		$this->form_validation->set_rules('alamat','Alamat','required'); 
		$this->form_validation->set_rules('keterangan','Keterangan','required');
		if($this->form_validation->run() != false){
			$data = array(  
				'nama_petugas' => $nama,
				'tempat_lahir_petugas' => $tempat,
				'tanggal_lahir_petugas' => $tanggal,
				'no_telp_petugas' => $no_telp,
				'alamat_petugas' => $alamat,
				'keterangan_petugas' => $keterangan
			);
			$this->M_petugas->insert_data($data,'tb_petugas');
			$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil disimpan.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');							 
			redirect(base_url().'Petugas');  
		}else{   	
			$this->load->view('Admin/Petugas/Tambah');   
		}
	}

	function Edit($id)
	{
		$where = array( 'id_petugas' => $id );
		$data['tb_petugas'] = $this->M_petugas->edit_data($where,'tb_petugas')->result();
		$this->load->view('Admin/Petugas/Edit',$data);
	}

	function Edit_petugas()
	{
		$id_petugas = $this->input->post('id_petugas');	
		$nama = $this->input->post('nama');
		$tempat = $this->input->post('tempat');
		$tanggal = $this->input->post('tanggal');
		$no_telp = $this->input->post('no_telp');
		$alamat = $this->input->post('alamat');
		$keterangan = $this->input->post('keterangan');

		$this->form_validation->set_rules('nama','Nama','required'); 
		$this->form_validation->set_rules('tempat','Tempat','required'); 
		$this->form_validation->set_rules('tanggal','Tanggal','required'); 
		$this->form_validation->set_rules('no_telp','No Telp','required'); 
		$this->form_validation->set_rules('alamat','Alamat','required'); 
		$this->form_validation->set_rules('keterangan','Keterangan','required'); 	
		if($this->form_validation->run() != false){
			$where = array( 'id_petugas' => $id_petugas );
			$data = array(  
				'nama_petugas' => $nama,
				'tempat_lahir_petugas' => $tempat,
				'tanggal_lahir_petugas' => $tanggal,
				'no_telp_petugas' => $no_telp,
				'alamat_petugas' => $alamat,
				'keterangan_petugas' => $keterangan
			);
			$this->M_petugas->update_data($where,$data,'tb_petugas');
			$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil diubah.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
			redirect(base_url().'Petugas');  
		}else{
			$where = array( 'id_petugas' => $id_petugas);   
			$data['tb_petugas'] = $this->M_petugas->edit_data($where,'tb_petugas')->result();     
			$this->load->view('Admin/Petugas/Edit',$data);
		}
	}

	function Delete($id)
	{
		$where = array('id_petugas' => $id);
		$this->M_petugas->delete_data($where,'tb_petugas');
		$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil dihapus.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
		redirect(base_url().'Petugas');
	}

	function Lihat($id)
	{
		$where = array( 'id_petugas' => $id );
		$data['tb_petugas'] = $this->M_petugas->edit_data($where,'tb_petugas')->result();
		$this->load->view('Admin/Petugas/Lihat',$data);
	}
}
