<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {
function __construct(){
		parent::__construct();
		$this->load->model('M_kategori');
		$this->form_validation->set_error_delimiters('<div style="color:#f9243f; margin-bottom: 5px; margin-top:2px">', '</div>');


//cek login
		if($this->session->userdata('status') != "admin_login"){ redirect(base_url().'login?pesan=belumlogin');
	}
}
	public function index()
	{

		$data=array('tb_pinjaman_kategori' => $this->M_kategori->get_kategori());
		$this->load->view('Admin/Kategori/kategori', $data);

	}
	function Batal()
	{
		$this->session->set_flashdata('notif','<div class="alert bg-warning" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Otorasi dibatalkan.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
		redirect('Anggota');
	}

	function Tambah()
	{
		$data['tb_pinjaman_kategori'] = $this->M_kategori->get_data('tb_pinjaman_kategori')->result();
		$this->load->view('Admin/Kategori/Tambah',$data);
	}

	function Tambah_kategori()
	{
		$nama = $this->input->post('nama');
		
		$this->form_validation->set_rules('nama','Nama','required');
		if($this->form_validation->run() != false){
			$data = array(  
				'nama_pinjaman_kategori' => $nama
				
			);
			$this->M_kategori->insert_data($data,'tb_pinjaman_kategori');
			$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil disimpan.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');							 
			redirect(base_url().'Kategori');  
		}else{   	
			$this->load->view('Admin/Kategori/Tambah');   
		}
	}

	function Edit($id)
	{
		$where = array( 'id_pinjaman_kategori' => $id );
		$data['tb_pinjaman_kategori'] = $this->M_kategori->edit_data($where,'tb_pinjaman_kategori')->result();
		$this->load->view('Admin/Kategori/Edit',$data);
	}

	function Edit_kategori()
	{
		$id_pinjaman_kategori = $this->input->post('id_pinjaman_kategori');	
		$nama = $this->input->post('nama');
		
		$this->form_validation->set_rules('nama','Nama','required'); 
		if($this->form_validation->run() != false){
			$where = array( 'id_pinjaman_kategori' => $id_pinjaman_kategori );
			$data = array(  
				'nama_pinjaman_kategori' => $nama
				);
			$this->M_kategori->update_data($where,$data,'tb_pinjaman_kategori');
			$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil diubah.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
			redirect(base_url().'Kategori');  
		}else{
			$where = array( 'id_pinjaman_kategori' => $id_pinjaman_kategori);   
			$data['tb_pinjaman_kategori'] = $this->M_kategori->edit_data($where,'tb_pinjaman_kategori')->result();     
			$this->load->view('Admin/Kategori/Edit',$data);
		}
	}

	function Delete($id)
	{
		$where = array('id_pinjaman_kategori' => $id);
		$this->M_kategori->delete_data($where,'tb_pinjaman_kategori');
		$this->session->set_flashdata('notif','<div class="alert bg-success" role="alert"><em class="fa fa-lg fa-warning">&nbsp;</em> <strong>Data berhasil dihapus.</strong> <a class="close pull-right" data-dismiss="alert" aria-label="Close"><em class="fa fa-lg fa-close"></em></a></div>');			
		redirect(base_url().'Kategori');
	}

	function Lihat($id)
	{
		$where = array( 'id_pinjaman_kategori' => $id );
		$data['tb_pinjaman_kategori'] = $this->M_kategori->edit_data($where,'tb_pinjaman_kategori')->result();
		$this->load->view('Admin/Kategori/Lihat',$data);
	}
}
