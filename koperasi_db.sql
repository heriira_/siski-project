-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2019 at 03:59 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `koperasi_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_anggota`
--

CREATE TABLE `tb_anggota` (
  `id_anggota` int(11) NOT NULL,
  `nama_anggota` varchar(150) NOT NULL,
  `alamat_anggota` text NOT NULL,
  `tanggal_lahir_anggota` date NOT NULL,
  `tempat_lahir_anggota` varchar(100) NOT NULL,
  `jenis_kelamin_anggota` varchar(200) NOT NULL COMMENT 'Pria, Wanita',
  `status_anggota` varchar(100) NOT NULL,
  `no_telp_anggota` varchar(14) NOT NULL,
  `keterangan_anggota` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_anggota`
--

INSERT INTO `tb_anggota` (`id_anggota`, `nama_anggota`, `alamat_anggota`, `tanggal_lahir_anggota`, `tempat_lahir_anggota`, `jenis_kelamin_anggota`, `status_anggota`, `no_telp_anggota`, `keterangan_anggota`) VALUES
(9, 'Siski', 'Sumedang', '2019-05-09', 'Sumedang', 'Wanita', 'PL', '0989034234', 'Pelajar'),
(10, 'Heri Irawan', 'Sumedang', '2019-05-09', 'Sumedang', 'Pria', 'JM', '098765432', 'oi');

-- --------------------------------------------------------

--
-- Table structure for table `tb_angsuran`
--

CREATE TABLE `tb_angsuran` (
  `id_angsuran` int(11) NOT NULL,
  `id_pinjaman` int(11) NOT NULL,
  `nama_peminjam` varchar(200) NOT NULL,
  `tanggal_pembayaran` date NOT NULL,
  `angsuran_ke` varchar(200) NOT NULL,
  `tanggal_jatuh_tempo` date NOT NULL,
  `besar_angsuran` varchar(200) NOT NULL,
  `keterangan_angsuran` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_angsuran`
--

INSERT INTO `tb_angsuran` (`id_angsuran`, `id_pinjaman`, `nama_peminjam`, `tanggal_pembayaran`, `angsuran_ke`, `tanggal_jatuh_tempo`, `besar_angsuran`, `keterangan_angsuran`) VALUES
(14, 7, 'Siski', '2019-05-09', 'Sipedes', '2019-05-17', '200000', 'Sip');

--
-- Triggers `tb_angsuran`
--
DELIMITER $$
CREATE TRIGGER `angsuran` AFTER INSERT ON `tb_angsuran` FOR EACH ROW BEGIN 
UPDATE tb_pinjaman set besar_pinjaman = besar_pinjaman - new.besar_angsuran WHERE id_pinjaman = new.id_pinjaman;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tb_petugas`
--

CREATE TABLE `tb_petugas` (
  `id_petugas` int(11) NOT NULL,
  `nama_petugas` varchar(100) NOT NULL,
  `alamat_petugas` text NOT NULL,
  `no_telp_petugas` varchar(14) NOT NULL,
  `tempat_lahir_petugas` varchar(100) NOT NULL,
  `tanggal_lahir_petugas` date NOT NULL,
  `keterangan_petugas` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_petugas`
--

INSERT INTO `tb_petugas` (`id_petugas`, `nama_petugas`, `alamat_petugas`, `no_telp_petugas`, `tempat_lahir_petugas`, `tanggal_lahir_petugas`, `keterangan_petugas`) VALUES
(4, 'Reira', 'Sumedang', '234567890098', 'Sumedang', '2001-06-01', 't');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pinjaman`
--

CREATE TABLE `tb_pinjaman` (
  `id_pinjaman` int(11) NOT NULL,
  `id_pinjaman_kategori` varchar(200) NOT NULL,
  `id_anggota` varchar(200) NOT NULL,
  `besar_pinjaman` varchar(200) NOT NULL,
  `tanggal_pengajuan_pinjaman` date NOT NULL,
  `tanggal_acc_peminjaman` date NOT NULL,
  `tanggal_pinjaman` date NOT NULL,
  `tanggal_pelunasan_pinjaman` date NOT NULL,
  `keterangan_pinjaman` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pinjaman`
--

INSERT INTO `tb_pinjaman` (`id_pinjaman`, `id_pinjaman_kategori`, `id_anggota`, `besar_pinjaman`, `tanggal_pengajuan_pinjaman`, `tanggal_acc_peminjaman`, `tanggal_pinjaman`, `tanggal_pelunasan_pinjaman`, `keterangan_pinjaman`) VALUES
(7, 'Sipedes', 'Siski', '300000', '2019-05-09', '2019-05-09', '2019-05-09', '2019-05-10', 'Sip'),
(8, 'Simpnjam', 'Heri Irawan', '500.000', '2019-05-10', '2019-05-11', '2019-05-10', '2019-05-13', 'Alal');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pinjaman_kategori`
--

CREATE TABLE `tb_pinjaman_kategori` (
  `id_pinjaman_kategori` int(11) NOT NULL,
  `nama_pinjaman_kategori` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pinjaman_kategori`
--

INSERT INTO `tb_pinjaman_kategori` (`id_pinjaman_kategori`, `nama_pinjaman_kategori`) VALUES
(1, 'Sipedes'),
(3, 'Simpnjam');

-- --------------------------------------------------------

--
-- Table structure for table `tb_simpanan`
--

CREATE TABLE `tb_simpanan` (
  `id_simpanan` int(11) NOT NULL,
  `nama_simpanan` varchar(150) NOT NULL,
  `id_anggota` varchar(200) NOT NULL,
  `tanggal_simpanan` date NOT NULL,
  `besar_simpanan` varchar(200) NOT NULL,
  `keterangan_simpanan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_simpanan`
--

INSERT INTO `tb_simpanan` (`id_simpanan`, `nama_simpanan`, `id_anggota`, `tanggal_simpanan`, `besar_simpanan`, `keterangan_simpanan`) VALUES
(1, 'Nabung', 'Siski', '2019-05-09', '500.000', 'Aokwk');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `id_petugas` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `nama_petugas` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`, `id_petugas`, `role_id`, `nama_petugas`) VALUES
(1, 'heri', '3b2285b348e95774cb556cb36e583106', 23, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Admin'),
(2, 'Member');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_anggota`
--
ALTER TABLE `tb_anggota`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indexes for table `tb_angsuran`
--
ALTER TABLE `tb_angsuran`
  ADD PRIMARY KEY (`id_angsuran`),
  ADD KEY `id_pinjaman` (`id_pinjaman`);

--
-- Indexes for table `tb_petugas`
--
ALTER TABLE `tb_petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `tb_pinjaman`
--
ALTER TABLE `tb_pinjaman`
  ADD PRIMARY KEY (`id_pinjaman`),
  ADD KEY `id_pinjaman_kategori` (`id_pinjaman_kategori`),
  ADD KEY `id_anggota` (`id_anggota`);

--
-- Indexes for table `tb_pinjaman_kategori`
--
ALTER TABLE `tb_pinjaman_kategori`
  ADD PRIMARY KEY (`id_pinjaman_kategori`);

--
-- Indexes for table `tb_simpanan`
--
ALTER TABLE `tb_simpanan`
  ADD PRIMARY KEY (`id_simpanan`),
  ADD KEY `id_anggota` (`id_anggota`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_anggota`
--
ALTER TABLE `tb_anggota`
  MODIFY `id_anggota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tb_angsuran`
--
ALTER TABLE `tb_angsuran`
  MODIFY `id_angsuran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tb_petugas`
--
ALTER TABLE `tb_petugas`
  MODIFY `id_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_pinjaman`
--
ALTER TABLE `tb_pinjaman`
  MODIFY `id_pinjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tb_pinjaman_kategori`
--
ALTER TABLE `tb_pinjaman_kategori`
  MODIFY `id_pinjaman_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_simpanan`
--
ALTER TABLE `tb_simpanan`
  MODIFY `id_simpanan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
